import { showMessageModal, showInputModal, showResultsModal } from "./views/modal.mjs";
import  { appendRoomElement } from "./views/room.mjs";
import  { appendUserElement, changeReadyStatus, setProgress, removeAllUsers } from "./views/user.mjs";

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

let currenRoom = null;
let gameText = '';
let nextCharIndex = 0;
let addedListener = false;

const socket = io('', { query: { username } }); 

socket.on('USER_ALREDY_EXIST', () => {
	showMessageModal({
		message: "User alredy exist", 
		onClose: () => {
			sessionStorage.removeItem('username');
			window.location.replace('/login');
		},
	});
});

socket.on('ROOM_ALREDY_EXIST', ({ roomId }) => {
	showMessageModal({ message: "Room name alredy exist" });
});

socket.on('CURRENT_ROOM', (room) => {
	currenRoom = room;
	render();
});

const roomWrapper = document.getElementById('rooms-wrapper');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
let roomName = document.getElementById('room-name');
const backToRooms = document.getElementById('quit-room-btn');
const readyBtn = document.getElementById('ready-btn');
const timerDiv = document.getElementById('timer');
const textContainer = document.getElementById('text-container');
const gameTimer = document.getElementById('game-timer');
const gameTimerSeconds = document.getElementById('game-timer-seconds');
const leftText = document.getElementById('left-text');
const centerText = document.getElementById('center-text');
const rightText = document.getElementById('right-text');

socket.on('ROOMS', (rooms) => {
	roomWrapper.innerHTML = '';

	const onJoinHandler = (roomId) => {
		socket.emit("JOIN_ROOM", roomId);
	}

	rooms.forEach((room) => {
      	appendRoomElement({ 
			name: room.name,
			numberOfUsers: `${room.users.length} user`,
			onJoin: () => onJoinHandler(room.name),
		});
	})
});

const createRoomButton = document.getElementById('add-room-btn');

const onSubmitInputRoom = (roomId) => {
	socket.emit('CREATE_ROOM', { roomId });

}
 
const createRoomHeandler = () => {
   showInputModal({title: 'Create a room name:', onSubmit: onSubmitInputRoom});
}

const backToRoomsHeandler = () => {
	socket.emit('LEAVE_ROOM');
	currenRoom = null;
	gameText = '';
	nextCharIndex = 0;
	unsetListener();
	render();
}

const clickReadyHeandler = () => {
	socket.emit('USER_READY');
}

const closeResultsModalHamdler = () => {
	currenRoom.status = 'WAIT';
	gameText = '';
	nextCharIndex = 0;
	render();
}

function keyupHandler (event) {
	const char = gameText[nextCharIndex];
	if (char === undefined) return;

	if (event.key === char) {
		nextCharIndex++;
		socket.emit('CHAR_INDEX', nextCharIndex);
	}
	render();
}

function setListener() {
	if (addedListener) return;
	document.addEventListener('keyup', keyupHandler);
	addedListener = true;
}


function unsetListener() {
	if (!addedListener) return;
	addedListener = false;
	document.removeEventListener('keyup', keyupHandler);
}

function render() {
	if (currenRoom) {
		if (currenRoom.textId && !gameText) {
			fetch(`http://localhost:3002/game/texts/${currenRoom.textId}`)
				.then(res => res.json())
				.then(({ data }) => gameText = data)
				.then(render);
		}
		roomsPage.classList.add('display-none');
		gamePage.classList.remove('display-none');
		roomName.innerHTML = currenRoom.name;

		removeAllUsers();
		currenRoom.users.forEach(user => {
			const progress = gameText.length ? user.charIndex * 100 / gameText.length : 0;

			const options = {
				username: user.name,
				ready: user.isReady,
				isCurrentUser: false,
				progress,
			}
			if (user.name === username) {
				options.isCurrentUser = true;
				readyBtn.textContent = user.isReady ? 'NOT READY' : 'READY';
				readyBtn.style.background = user.isReady ? '#e81224' : '#16c60c';
			}
			appendUserElement(options);
		});
		
		if (currenRoom.status === 'WAIT') {
			backToRooms.hidden = false;
			readyBtn.hidden = false;
		} else {
			backToRooms.hidden = true;
			readyBtn.hidden = true;
		}

		if (currenRoom.status === 'START') {
			timerDiv.classList.remove('display-none');
			timerDiv.textContent = currenRoom.startIn;
		} else {
			timerDiv.classList.add('display-none');
		}

		if (currenRoom.status === 'IN_PROGRESS') {
			textContainer.classList.remove('display-none');
			gameTimer.classList.remove('display-none');

			leftText.textContent = gameText.substring(0, nextCharIndex);
			centerText.textContent = gameText.substring(nextCharIndex, nextCharIndex + 1);
			rightText.textContent = gameText.substring(nextCharIndex + 1);
			
			gameTimerSeconds.textContent = currenRoom.endIn;
			setListener();
		} else {
			textContainer.classList.add('display-none');
			gameTimer.classList.add('display-none');
			unsetListener();
		}


		if (currenRoom.status === 'FINISHED') {
			showResultsModal({ usersSortedArray: currenRoom.result , onClose: closeResultsModalHamdler });
		} else {
			// todo
		}

	} else {
		roomsPage.classList.remove('display-none');
		gamePage.classList.add('display-none');
	}
}

createRoomButton.addEventListener('click', createRoomHeandler);
backToRooms.addEventListener('click', backToRoomsHeandler)
readyBtn.addEventListener('click', clickReadyHeandler)