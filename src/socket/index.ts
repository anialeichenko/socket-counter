import { Server } from 'socket.io';

import { texts } from '../data';
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME  } from './config';

const roomsMap = new Map();
/*
	{
		room1: {
			name: room1
			users: {
				anna: {
					name: 'anna',
					isReady: false,
				},
				max: {
					name: 'max',
					isReady: false,
				}
			}
		},
	}
*/
const namesSet = new Set();

function randomIntFromInterval(min, max) { // min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min)
}

const getCurrentRoomId = (socket) => Array.from(socket.rooms.values()).find(roomId => roomsMap.has(roomId));

export default (io: Server) => {

	const sendRooms = () => {
		const rooms = Array.from(roomsMap.values());
		const filteredRooms = rooms.filter((room) => room.get('users').size < MAXIMUM_USERS_FOR_ONE_ROOM && room.get('status') === 'WAIT');
		const res = filteredRooms.map(roomMap => {
			return {
				name: roomMap.get('name'),
				status: roomMap.get('status'),
				users: Array.from(roomMap.get('users').values()),
			};
		}) 
		io.emit('ROOMS', res); 
	}

	const sendRoom = (roomId) => {
		const roomMap = roomsMap.get(roomId);
		if (!roomMap) return;
		const res = {
			name: roomMap.get('name'),
			status: roomMap.get('status'),
			startIn: roomMap.get('startIn'),
			endIn: roomMap.get('endIn'),
			textId: roomMap.get('textId'),
			result: roomMap.get('result'),
			users: Array.from(roomMap.get('users').values()),
		};
		io.to(roomId).emit('CURRENT_ROOM', res); 
	}

	const leaveRoom = ({ roomId, username }) => {
		const room = roomsMap.get(roomId);
		const users = room.get('users');
		users.delete(username);
		if (users.size === 0) {
			roomsMap.delete(roomId);
		}
	}

	const joinRoom = ({ socket, roomId, username }) => {
		const prevRoomId = getCurrentRoomId(socket);

		if (roomId === prevRoomId) {
			return;
		}
		if (prevRoomId) {
			socket.leave(prevRoomId);
			leaveRoom({ roomId: prevRoomId, username });
		}
		// @ts-ignore
		socket.join(roomId);

		const room = roomsMap.get(roomId);
	
		if (room.get('users').size < MAXIMUM_USERS_FOR_ONE_ROOM) {
			room.get('users').set(username, {
				name: username,
				isReady: false,
				charIndex: 0,
				finishedAt: null,
			});
			sendRoom(roomId);
		}		
	}

	const startGame = ({ roomId }) => {
		const roomMap = roomsMap.get(roomId);
		if (!roomMap) return;
		
		if (roomMap.get('status') !== 'WAIT') return;

		const users = roomMap.get('users');
		if (users.size < 2) return;

		const usersArr = Array.from(users.values());
		// @ts-ignore
		if (usersArr.some(user => user.isReady === false)) return;
		roomMap.set('status', 'START');
		roomMap.set('textId', randomIntFromInterval(0, texts.length - 1));
		sendRoom(roomId);

		const gameStarted = () => {
			const intervalId = setInterval(() => {
				const endIn = roomMap.get('endIn');
				roomMap.set('endIn', endIn - 1);
				finishGame({ roomId });
				sendRoom(roomId);
			}, 1000);
			roomMap.set('intervalId', intervalId);
		}

		const intervalId = setInterval(() => {
			const startIn = roomMap.get('startIn');
			if (startIn === 0) {
				roomMap.set('status', 'IN_PROGRESS');
				clearInterval(intervalId);
				gameStarted();
			} else {
				roomMap.set('startIn', startIn - 1);
			}
			sendRoom(roomId);
		}, 1000);
	}

	const finishGame = ({ roomId }) => {
		const roomMap = roomsMap.get(roomId);
		if (!roomMap) return;
		
		if (roomMap.get('status') !== 'IN_PROGRESS') return;

		const users = roomMap.get('users');
		const textLength = texts[roomMap.get('textId')].length;
		const endIn = roomMap.get('endIn');
		const usersArr = Array.from(users.values());
		// @ts-ignore
		if (users.size >= 2 && usersArr.some(user => user.charIndex < textLength) && endIn !== 0) return;
	
		// @ts-ignore
		const finishedUsers = usersArr.filter(user => user.finishedAt).sort((a, b) => new Date(a.finishedAt) - new Date(b.finishedAt)).map(user => user.name);
		// @ts-ignore
		const unfinishedUsers = usersArr.filter(user => !user.finishedAt).sort((a, b) => a.charIndex - b.charIndex).map(user => user.name);
		const usersSortedArray = [...finishedUsers, ...unfinishedUsers];
		
		usersArr.forEach(user => {
			// @ts-ignore
			user.isReady = false;
			// @ts-ignore
			user.finishedAt = null;
			// @ts-ignore
			user.charIndex = 0;
		});
		roomMap.set('status', 'FINISHED');
		roomMap.set('result', usersSortedArray);
		roomMap.set('startIn', SECONDS_TIMER_BEFORE_START_GAME);
		roomMap.set('endIn', SECONDS_FOR_GAME);
		roomMap.set('textId', null),
		clearInterval(roomMap.get('intervalId'));
		sendRoom(roomId);
	};

	io.on('connection', socket => {
		const username = socket.handshake.query.username;

		if (namesSet.has(username)) {
			socket.emit('USER_ALREDY_EXIST');
			return;
		}
		namesSet.add(username);
		socket.emit('USER_CREATED');
		
		sendRooms();

		socket.on('CREATE_ROOM', ({ roomId }) => {
			if (roomsMap.has(roomId)) {
				socket.emit('ROOM_ALREDY_EXIST', { roomId });
				return;
			}
			if(roomId === '') {
				return;
			}
			const room = new Map();
			room.set('status', 'WAIT');
			room.set('intervalId', null);
			room.set('startIn', SECONDS_TIMER_BEFORE_START_GAME);
			room.set('endIn', SECONDS_FOR_GAME);
			room.set('textId', null);
			room.set('name', roomId);
			room.set('users', new Map());
  			roomsMap.set(roomId, room);
			joinRoom({ socket, roomId, username });
			sendRooms();
		});

		socket.on("JOIN_ROOM", roomId => {
			joinRoom({ socket, roomId, username });
			startGame({ roomId });
			sendRooms();
		});

		socket.on("LEAVE_ROOM", () => {
			const roomId = getCurrentRoomId(socket);

			if (roomId) {
				const roomMap = roomsMap.get(roomId);
				if (roomMap.get('status') === 'FINISHED') {
					roomMap.set('status', 'WAIT');
					roomMap.set('result', null);
				}
				leaveRoom({ roomId, username });
				startGame({ roomId });
				finishGame({ roomId });
				sendRoom({ roomId });
				sendRooms();
				// @ts-ignore
				socket.leave(roomId);
			}
		});


		socket.on("USER_READY", () => {
			const roomId = getCurrentRoomId(socket);
			if (roomId) {
				const roomMap = roomsMap.get(roomId);
				if (roomMap.get('status') === 'FINISHED') {
					roomMap.set('status', 'WAIT');
					roomMap.set('result', null);
				}
				const user = roomMap.get('users').get(username);
				user.isReady = !user.isReady;
				startGame({ roomId });
				sendRoom(roomId);
			}
		});

		socket.on("CHAR_INDEX", (charIndex) => {
			const roomId = getCurrentRoomId(socket);
			if (roomId) {
				const roomMap = roomsMap.get(roomId);
				if (roomMap.get('status') !== 'IN_PROGRESS') return;
				const user = roomMap.get('users').get(username);
				// @ts-ignore
				user.charIndex = charIndex;
				if (charIndex === texts[roomMap.get('textId')].length) {
					user.finishedAt = new Date();
				}
				finishGame({ roomId });
			}
		});

		socket.on("disconnecting", () => {
			namesSet.delete(username);
			const roomId = getCurrentRoomId(socket);

			if (roomId) {
				leaveRoom({ roomId, username });
				startGame({ roomId });
				finishGame({ roomId });
				sendRooms();
			}
		});
	});
	
};
